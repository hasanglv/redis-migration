package com.hasan.redistransfer.service;

import io.lettuce.core.RedisClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class RedisMigrationService {

    private final RedisTemplate<String, Object> sourceRedisTemplate;
    private final RedisTemplate<String, Object> destinationRedisTemplate;

    public RedisMigrationService(
            @Qualifier("sourceRedisTemplate") RedisTemplate<String, Object> sourceRedisTemplate,
            @Qualifier("destinationRedisTemplate") RedisTemplate<String, Object> destinationRedisTemplate) {
        this.sourceRedisTemplate = sourceRedisTemplate;
        this.destinationRedisTemplate = destinationRedisTemplate;
    }

    public void populateSourceDatabase() {
        // Populate hash data type
        for (int i = 1; i <= 100; i++) {
            sourceRedisTemplate.opsForHash().put("hashKey" + i, "field" + i, "value" + i);
        }

        // Populate string data type
        for (int i = 1; i <= 100; i++) {
            sourceRedisTemplate.opsForValue().set("stringKey" + i, "stringValue" + i);
        }

        // Populate list data type
        for (int i = 1; i <= 100; i++) {
            sourceRedisTemplate.opsForList().leftPush("listKey" + i, "listValue" + i);
        }

        // Populate set data type
        for (int i = 1; i <= 100; i++) {
            sourceRedisTemplate.opsForSet().add("setKey" + i, "setValue" + i);
        }

        // Populate sorted set data type
        for (int i = 1; i <= 100; i++) {
            sourceRedisTemplate.opsForZSet().add("zSetKey  " + i, "zSetValue" + i, i);
        }
    }

    public void migrateData(String sourceHost, int sourcePort, String destinationHost, int destinationPort) {
        for (int i = 1; i <= 100; i++) {
            String hashKey = "hashKey" + i;
            Map<Object, Object> entries = sourceRedisTemplate.opsForHash().entries(hashKey);
            if (!entries.isEmpty()) {
                destinationRedisTemplate.opsForHash().putAll(hashKey, entries);
                destinationRedisTemplate.expire(hashKey, 30, TimeUnit.DAYS);
            }

            String stringKey = "stringKey" + i;
            String stringValue = (String) sourceRedisTemplate.opsForValue().get(stringKey);
            if (stringValue != null) {
                destinationRedisTemplate.opsForValue().set(stringKey, stringValue);
                destinationRedisTemplate.expire(stringKey, 30, TimeUnit.DAYS);
            }

            String listKey = "listKey" + i;
            String listValue = (String) sourceRedisTemplate.opsForList().leftPop(listKey);
            if (listValue != null) {
                destinationRedisTemplate.opsForList().leftPush(listKey, listValue);
                destinationRedisTemplate.expire(listKey, 30, TimeUnit.DAYS);
            }

            String setKey = "setKey" + i;
            String setValue = (String) sourceRedisTemplate.opsForSet().pop(setKey);
            if (setValue != null) {
                destinationRedisTemplate.opsForSet().add(setKey, setValue);
                destinationRedisTemplate.expire(setKey, 30, TimeUnit.DAYS);
            }

            String zSetKey = "zSetKey" + i;
            Double score = sourceRedisTemplate.opsForZSet().score(zSetKey, "zSetValue" + i);
            if (score != null) {
                destinationRedisTemplate.opsForZSet().add(zSetKey, "zSetValue" + i, score);
                destinationRedisTemplate.expire(zSetKey, 30, TimeUnit.DAYS);
            }
        }
    }
}