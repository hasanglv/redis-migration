package com.hasan.redistransfer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisTransferApplication {

    public static void main(String[] args) {

        SpringApplication.run(RedisTransferApplication.class, args);
    }

}
