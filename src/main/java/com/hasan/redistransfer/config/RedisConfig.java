package com.hasan.redistransfer.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    @Value("${spring.redis.source.host}")
    private String sourceRedisHost;

    @Value("${spring.redis.source.port}")
    private int sourceRedisPort;

    @Value("${spring.redis.destination.host}")
    private String destinationRedisHost;

    @Value("${spring.redis.destination.port}")
    private int destinationRedisPort;

    @Bean(name = "sourceRedisConnectionFactory")
    public RedisConnectionFactory sourceRedisConnectionFactory() {
        return new LettuceConnectionFactory(sourceRedisHost, sourceRedisPort);
    }

    @Bean(name = "destinationRedisConnectionFactory")
    public RedisConnectionFactory destinationRedisConnectionFactory() {
        return new LettuceConnectionFactory(destinationRedisHost, destinationRedisPort);
    }

    @Bean(name = "sourceRedisTemplate")
    public RedisTemplate<String, Object> sourceRedisTemplate(@Qualifier("sourceRedisConnectionFactory") RedisConnectionFactory sourceRedisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(sourceRedisConnectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericToStringSerializer<>(Object.class));
        return template;
    }

    @Bean(name = "destinationRedisTemplate")
    public RedisTemplate<String, Object> destinationRedisTemplate(@Qualifier("destinationRedisConnectionFactory") RedisConnectionFactory destinationRedisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(destinationRedisConnectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericToStringSerializer<>(Object.class));
        return template;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(@Qualifier("sourceRedisConnectionFactory") RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericToStringSerializer<>(Object.class));
        return template;
    }
}