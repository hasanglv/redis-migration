package com.hasan.redistransfer.controller;

import com.hasan.redistransfer.service.RedisMigrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redis")
@RequiredArgsConstructor
public class RedisMigrationController {

    private final RedisMigrationService redisMigrationService;

    @GetMapping("/populate")
    public String populateSourceDatabase() {
        redisMigrationService.populateSourceDatabase();
        return "Source Redis database populated with sample data!";
    }

    @GetMapping("/migrate")
    public String dataTransfer(@RequestHeader String host, @RequestHeader int hostPort,
                               @RequestHeader String destination, @RequestHeader int destinationPort) {
        redisMigrationService.migrateData(host, hostPort, destination, destinationPort);
        return "Data migration completed!";
    }
}